import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/weather",
      name: "weather",
      component: () => import("../views/WeatherView.vue"),
    },
    {
      path: "/agenda",
      name: "agenda",
      component: () => import("../views/AgendaView.vue"),
    },
    {
      path: "/birthday",
      name: "birthday",
      component: () => import("../views/BirthdayView.vue"),
    },
    {
      path: "/todo",
      name: "todo",
      component: () => import("../views/TodoView.vue"),
    },
  ],
});

export default router;
